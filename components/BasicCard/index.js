import React from "react";
import { Text, View, Image } from "react-native";
import styles from "./style";
const BasicCard = ({ item }) => (
  <View style={styles.itemContainer}>
    <Image
      source={{
        uri: item.url,
      }}
      style={styles.image}
    />
    <Text style={styles.itemName}>{item.name}</Text>
    <Text style={styles.itemDescription}>{item.description}</Text>
  </View>
);

export default BasicCard;
