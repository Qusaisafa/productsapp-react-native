import { StyleSheet } from "react-native";

export default StyleSheet.create({
  image: {
    width: 50,
    height: 50,
  },
  itemContainer: {
    justifyContent: "flex-end",
    backgroundColor: "white",
    borderRadius: 5,
    padding: 10,
    height: 120,
  },
  itemName: {
    paddingTop: 5,
    fontSize: 16,
    color: "#000",
    fontWeight: "600",
  },
  itemDescription: {
    paddingTop: 5,
    fontWeight: "600",
    fontSize: 12,
    color: "#666",
  },
});
