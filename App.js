import React from "react";
import MainApp from "./routes";

export default function App() {
  return <MainApp />;
}
