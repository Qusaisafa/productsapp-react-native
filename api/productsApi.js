export const getAllProdcuts = async () => {
  try {
    const response = await fetch("http://localhost:3000/items");
    const json = await response.json();
    return json;
  } catch (error) {
    console.error(error);
  } finally {
  }
};

export const uploadPhoto = async (file) => {
  try {
    let data = new FormData();
    data.append("image", file);
    const response = await fetch("http://localhost:3000/upload", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: data,
    });
  } catch (error) {
    console.log(error);
    alert("Please Select File first");
    return null;
  }
  alert("Upload Successful");
  const json = await response.json();
  return "http://localhost:3000/" + json.filename;
};

export const createProduct = async (values) => {
  try {
    const response = await fetch("http://localhost:3000/items", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: values.name,
        description: values.description,
        quantity: 20,
        url: values.url,
      }),
    });
  } catch (error) {
    console.log(error);
    return null;
  }
  const json = await response.json();
  return json;
};
