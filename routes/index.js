import React from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Home from "../views/Home";
import Login from "../views/Login";
import SignUp from "../views/Signup";
import Products from "../views/Products";
import NewProduct from "../views/NewProduct";
const Stack = createNativeStackNavigator();

export default ({ params }) => (
  <SafeAreaView style={style.Container}>
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Login"
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen
          name={"Home"}
          component={Home}
          options={{
            headerShown: true,
            headerBackTitleVisible: false,
            headerBackButtonMenuEnabled: false,
            title: "My home",
            headerStyle: {
              backgroundColor: "#f4511e",
            },
            headerTintColor: "#fff",
            headerTitleStyle: {
              fontWeight: "bold",
            },
          }}
        />
        <Stack.Screen
          name={"Products"}
          component={Products}
          options={{
            headerShown: true,
            title: "My Products",
            headerStyle: {
              backgroundColor: "#f4511e",
            },
            headerTintColor: "#fff",
            headerTitleStyle: {
              fontWeight: "bold",
            },
          }}
        />
        <Stack.Screen
          name={"NewProduct"}
          component={NewProduct}
          options={{
            headerShown: true,
            title: "New Product",
            headerStyle: {
              backgroundColor: "#f4511e",
            },
            headerTintColor: "#fff",
            headerTitleStyle: {
              fontWeight: "bold",
            },
          }}
        />
        <Stack.Screen name={"Login"} component={Login} />
        <Stack.Screen name={"SignUp"} component={SignUp} />
      </Stack.Navigator>
    </NavigationContainer>
  </SafeAreaView>
);

const style = StyleSheet.create({
  Container: {
    flex: 1,
  },
});
