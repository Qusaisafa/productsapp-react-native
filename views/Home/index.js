import React from "react";
import { Text, View } from "react-native";
import BasicButton from "../../components/BasicButton";

const Home = ({ navigation }) => (
  <View>
    <BasicButton
      title={"Products"}
      onPress={() => navigation.navigate("Products")}
      color="transparent"
      type="clear"
    />
    <BasicButton
      title={"New Products"}
      onPress={() => navigation.navigate("NewProduct")}
      color="transparent"
      type="clear"
    />
  </View>
);

export default Home;
