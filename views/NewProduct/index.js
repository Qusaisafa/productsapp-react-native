import React, { useState } from "react";
import { Text, View } from "react-native";
import { useFormik } from "formik";

import BasicInput from "../../components/BasicInput";
import BasicButton from "../../components/BasicButton";

import validationSchema from "./schema";
import styles from "./styles";
import { createProduct } from "./../../api/productsApi";
import { uploadPhoto } from "./../../api/productsApi";
import DocumentPicker from "react-native-document-picker";
import { TouchableOpacity } from "react-native-gesture-handler";

const initialValues = {
  name: "",
  url: "",
  description: "",
};

const NewProduct = ({ navigation }) => {
  const [singleFile, setSingleFile] = useState(null);

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit,
  });

  const {
    values,
    touched,
    errors,
    handleChange,
    isSubmitting,
    isValid,
    handleSubmit,
  } = formik;

  const saveProduct = async (values) => {
    await createProduct(values);
  };

  const uploadNewPhoto = async () => {
    // setTimeout(() => {
    //   navigation.navigate("Home");
    // }, 3000);
    const res = await uploadPhoto(singleFile);
    setTimeout(3000);
    formik.setFieldValue("url", res);
  };

  const onSubmit = async (values) => {
    const response = await createProduct(values);
    if (response !== null) {
      console.log("saved new product", response);
    } else {
    }
  };

  const selectFile = async () => {
    // Opening Document Picker to select one file
    try {
      const res = await DocumentPicker.pick({
        // Provide which type of file you want user to pick
        type: [DocumentPicker.types.images],
        // There can me more options as well
        // DocumentPicker.types.allFiles
        // DocumentPicker.types.images
        // DocumentPicker.types.plainText
        // DocumentPicker.types.audio
        // DocumentPicker.types.pdf
      });
      // Printing the log realted to the file
      console.log("res : " + JSON.stringify(res));
      // Setting the state to show single file attributes
      setSingleFile(res);
    } catch (err) {
      setSingleFile(null);
      // Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        // If user canceled the document selection
        alert("Canceled");
      } else {
        // For Unknown Error
        alert("Unknown Error: " + JSON.stringify(err));
        throw err;
      }
    }
  };

  return (
    <View style={styles.Container}>
      <BasicInput
        placeholder={"Enter product name"}
        iconName="user"
        iconSize={20}
        onChangeText={handleChange("name")}
        value={values.name}
        errorMessage={touched.name && errors.name}
      />
      <BasicInput
        placeholder={"Enter product descriptions"}
        iconName="user"
        iconSize={20}
        onChangeText={handleChange("description")}
        value={values.description}
        errorMessage={touched.description && errors.description}
      />
      <TouchableOpacity
        style={styles.buttonStyle}
        activeOpacity={0.5}
        onPress={selectFile}
      >
        <Text style={styles.buttonTextStyle}>Select File</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.buttonStyle}
        activeOpacity={0.5}
        onPress={uploadNewPhoto}
      >
        <Text style={styles.buttonTextStyle}>Upload File</Text>
      </TouchableOpacity>

      <BasicButton
        title={"save"}
        width={200}
        onPress={handleSubmit}
        disabled={!isValid || isSubmitting}
        loading={isSubmitting}
      />
    </View>
  );
};

export default NewProduct;
