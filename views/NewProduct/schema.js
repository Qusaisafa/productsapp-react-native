import * as Yup from "yup";

export default Yup.object().shape({
  name: Yup.string()
    .label("Name")
    .required()
    .min(2, "Must have at least 2 characters"),
  description: Yup.string()
    .label("Description")
    .required()
    .min(2, "Must have at least 2 characters"),
  url: Yup.string()
    .label("url")
    .url("Enter a valid url")
    .required("Please enter a valid url"),
});
