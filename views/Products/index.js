import React, { useEffect, useState } from "react";
import BasicCard from "./../../components/BasicCard";
import { getAllProdcuts } from "../../api/productsApi";
import { FlatList, SafeAreaView, StyleSheet, View, Text } from "react-native";
import styles from "./style";
import { FlatGrid } from "react-native-super-grid";
const Products = () => {
  const [products, setProducts] = useState([]);

  // call get Items
  const getAllProducts = async () => {
    const products = await getAllProdcuts();
    setProducts(products);
  };

  // Use effect
  useEffect(() => {
    getAllProducts();
  }, []);

  return (
    <FlatGrid
      itemDimension={130}
      data={products}
      style={styles.gridView}
      // staticDimension={300}
      // fixed
      spacing={10}
      renderItem={({ item }) => <BasicCard item={item} />}
    />
  );
};
export default Products;
