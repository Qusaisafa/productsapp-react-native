import { StyleSheet } from "react-native";

export default StyleSheet.create({
  gridView: {
    marginTop: 10,
    flex: 1,
  },
});
